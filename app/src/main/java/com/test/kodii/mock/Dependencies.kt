package com.test.kodii.mock

/**
 * Simulated DAO for user data.
 */
class UserDao {

    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
}

/**
 * Simulated DAO for state data.
 */
class StateDao {

    var code: Int? = 0
}

/**
 * Simulated authenticator.
 */
class Auth(
        val userDao: UserDao,
        val stateDao: StateDao,
        val debug: Boolean = false) {

    /**
     * Simulated authentication routine.
     */
    fun authenticate() {
        if (debug) return

        with(userDao) {
            firstName = "Joe"
            lastName = "Bloggs"
            email = "jbloggs@example.com"
        }
        stateDao.code = 1
    }
}
