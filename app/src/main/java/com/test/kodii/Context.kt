package com.test.kodii

import com.test.kodii.di.MainDataModule
import com.test.kodii.di.MainLogicModule
import com.test.kodii.di.Module


/**
 * Basic concept of application "context".
 */
open class Context {

    @Suppress("LeakingThis")
    open val module = Module(
            data = MainDataModule(),
            logic = MainLogicModule()
    )
            .transitive()
}
