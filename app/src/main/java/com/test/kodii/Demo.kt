package com.test.kodii

import com.test.kodii.di.Module


fun main(args: Array<String>) {
    Demo(Context()).run()
}

/**
 * Demo app definition.
 */
class Demo(context: Context) : Injects<Module> {

    var userDao by required { userDao }
    val stateDao by required { stateDao }
    val auth by required { auth }

    init {
        inject(context.module)
    }

    fun run() {
        auth.authenticate()
        println("First name: ${userDao.firstName}")
        println("Last name: ${userDao.lastName}")
        println("Emails: ${userDao.email}")
        println("State: ${stateDao.code}")
    }
}
