package com.test.kodii.di

import com.test.kodii.HasModules
import com.test.kodii.mock.Auth
import com.test.kodii.mock.StateDao
import com.test.kodii.mock.UserDao


/**
 * Application module.
 */
class Module(
    data: DataModule,
    logic: LogicModule
) :
        DataModule by data,
        LogicModule by logic,
    HasModules {

    override val modules = setOf(data, logic)
}

/**
 * Module for data providers.
 */
interface DataModule {

    val userDao: UserDao
    val stateDao: StateDao
}

/**
 * Module for logic objects.
 */
interface LogicModule {

    val auth: Auth
}
