package com.test.kodii.di

import com.test.kodii.Injects
import com.test.kodii.mock.Auth
import com.test.kodii.mock.StateDao
import com.test.kodii.mock.UserDao
import com.test.kodii.required


/**
 * Main implementation of [DataModule].
 */
class MainDataModule : DataModule {

    override val stateDao = StateDao()
    override val userDao = UserDao()
}

/**
 * Main implementation of [LogicModule].
 */
class MainLogicModule : LogicModule, Injects<DataModule> {

    private val stateDao by required { stateDao }
    private val userDao by required { userDao }

    override val auth get() = Auth(userDao, stateDao)
}
