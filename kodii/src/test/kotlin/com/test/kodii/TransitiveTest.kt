
package com.test.kodii

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Test case for transitive dependencies.
 */
class TransitiveTest {

    @Test fun testCast_legal() {
        try {
            Module1(
                    object : ModuleA {
                        override val stringA = "A"
                    },
                    object : ModuleB {
                        val stringA by required { stringA }
                        override val stringB get() = "${stringA}B"
                    }
            ).transitive()
            assertTrue(true)
        } catch (e: TransitiveInjectionException) {
            assertTrue(false)
        }
    }

    @Test fun testCast_illegal() {
        try {
            Module2(
                    object : ModuleA {
                        override val stringA = "A"
                    },
                    object : ModuleC {
                        val stringA by required { stringA }
                        override val stringC get() = "${stringA}C"
                    }
            ).transitive()
            assertTrue(false)
        } catch (e: TransitiveInjectionException) {
            assertTrue(true)
        }
    }

    @Test fun testCast_valid() {
        val a = object : ModuleA {
            override val stringA = "A"
        }
        val b = object : ModuleB {
            val stringA by required { stringA }
            override val stringB get() = "${stringA}B"
        }
        Module1(a, b).transitive()
        assertEquals("A", a.stringA)
        assertEquals("A", b.stringA)
        assertEquals("AB", b.stringB)
    }
}

interface ModuleA {
    val stringA: String
}

interface ModuleB : Injects<ModuleA> {
    val stringB: String
}

interface ModuleC : Injects<RogueModule> {
    val stringC: String
}

interface RogueModule {
    val stringA: String
}

class Module1(
        a: ModuleA,
        b: ModuleB) :
        ModuleA by a,
        ModuleB by b,
        HasModules {

    override val modules = setOf(a, b)
}

class Module2(
        a: ModuleA,
        c: ModuleC) :
        ModuleA by a,
        ModuleC by c,
        HasModules {

    override val modules = setOf(a, c)
}
