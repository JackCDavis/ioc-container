package com.test.kodii

import com.test.kodii.utils.CallerMap

/**
 * Static storage of [Kodii] instances.
 */
object Kodiis {

    internal val instances = CallerMap<Injects<*>, Kodii<*>>()

    /**
     * Retrieves active instance of [Kodii] or creates a new one.
     *
     * @param caller Injection caller instance, used as lookup key.
     * @return Stored or new instance.
     */
    fun <M> get(caller: Injects<M>) = fetch(caller) ?: Kodii<M>().apply { instances[caller] = this }

    /**
     * Fetches stored instance.
     *
     * @param caller Injection caller instance, used as lookup key.
     * @return Stored instance or null.
     */
    @Suppress("UNCHECKED_CAST")
    internal fun <M> fetch(caller: Injects<M>) = instances[caller] as? Kodii<M>
}
