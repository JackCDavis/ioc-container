package com.test.kodii

import java.lang.ClassCastException
import java.util.*

/**
 * Module with other submodules attached via delegation.
 */
interface HasModules {

    /**
     * Set of child modules. Used for transitive injection.
     */
    val modules: Set<Any>
}

/**
 * Injects root module into any submodules with transitive dependencies.
 *
 * @return Root module for chaining.
 * @param <M> Module type, must have submodules.
 */
fun <M : HasModules> M.transitive(): M {
    val queue = ArrayDeque<Any?>()
    queue.offer(this)
    while (queue.isNotEmpty()) {
        val submodule = queue.poll()!!
        try {
            @Suppress("UNCHECKED_CAST")
            (submodule as? Injects<M>)?.inject(this)
            (submodule as? HasModules)?.let { queue.addAll(it.modules) }
        } catch (e: ClassCastException) {
            throw TransitiveInjectionException(submodule::class.java)
        }
    }
    return this
}

/**
 * Class cast exception indicating that a submodule depends on a module that
 * is not a descendant of the root module.
 *
 * @param
 */
class TransitiveInjectionException(clazz: Class<*>) : ClassCastException("Unexpected transitive injection: ${clazz.name}")
