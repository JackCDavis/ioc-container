package com.test.kodii.utils

import java.util.*

/**
 * Utility map for storing caller instances.
 *
 * Implements a cache such that the most recent entry is returned at constant time.
 */
internal class CallerMap<K, V> : WeakHashMap<K, V>() {

    @Volatile internal var lastKey: K? = null
    @Volatile internal var lastValue: V? = null

    override fun containsKey(key: K) = key == lastKey || super.containsKey(key)

    override fun containsValue(value: V): Boolean {
        return super.containsValue(value)
    }

    operator override fun get(key: K): V? {
        return if (key == lastKey) {
            lastValue
        } else {
            val value = super.get(key)
            setLast(key, value)
            return value
        }
    }

    operator fun set(key: K, value: V) = put(key, value)

    override fun put(key: K, value: V): V? {
        setLast(key, value)
        return super.put(key, value)
    }

    override fun remove(key: K): V? {
        if (key == lastKey) {
            setLast()
        }
        return super.remove(key)
    }

    override fun clear() {
        super.clear()
        setLast()
    }

    /**
     * Set last key-value pair cache.
     *
     * @param key Map key.
     * @param value Map value.
     */
    private fun setLast(key: K? = null, value: V? = null) {
        lastKey = key
        lastValue = value
    }
}
