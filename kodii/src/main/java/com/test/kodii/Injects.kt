@file:Suppress("unused")

package com.test.kodii

/**
 * Injection interface.
 */
interface Injects<M>

/**
 * Fetches [Kodii] instance and calls [Kodii.required].
 */
fun <M, T> Injects<M>.required(initializer: M.() -> T) = Kodiis.get(this).required(initializer)

/**
 * Fetches [Kodii] instance and calls [Kodii.optional].
 */
fun <M, T> Injects<M>.optional(initializer: M.() -> T?) = Kodiis.get(this).optional(initializer)

/**
 * Fetches [Kodii] instance and calls [Kodii.inject].
 */
fun <M> Injects<M>.inject(module: M) {
    Kodiis.get(this).inject(module)
}
