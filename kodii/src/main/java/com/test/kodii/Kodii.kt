package com.test.kodii

/**
 * Kodii injector.
 *
 * @param <M> Module type that it injects.
 */
class Kodii<M> {

    internal val delegates = mutableListOf<Delegate<M, *>>()

    /**
     * Initializes property delegates with provided functions.
     *
     * @param module Injection module containing providers.
     */
    @Synchronized fun inject(module: M) {
        delegates.forEach { it.initialize(module) }
    }

    /**
     * Creates and registers delegate for a required (non-null) injectable property.
     *
     * @param initializer Initializer function from the module context to value.
     * @return Required (non-null) property delegate.
     */
    fun <T> required(initializer: M.() -> T) = Delegate.Required(initializer).apply { delegates += this }

    /**
     * Creates and registers delegate for an optional (nullable) injectable property.
     *
     * @param initializer Initializer function from the module context to value.
     * @return Optional (nullable) property delegate.
     */
    fun <T> optional(initializer: M.() -> T?) = Delegate.Optional(initializer).apply { delegates += this }

    /**
     * Shortcut for [required] by invoking the class like a function.
     *
     * @param initializer Initializer function from the module context to value.
     * @return Required (non-null) property delegate.
     */
    operator fun <T> invoke(initializer: M.() -> T) = required(initializer)
}
